'use strict';

const Web3 = require('web3');
const web3 = new Web3('http://localhost:8545');
const bip39 = require('bip39');
const makeUnionWallet = require('../lib/mock-web3');

describe('union wallet', () => {
	it('should make a tezos wallet', async () => {
    const mockMnemonic = bip39.generateMnemonic();
		const wallet = await makeUnionWallet(web3, mockMnemonic);
		console.log(wallet);
	});
});
