'use strict';

const makeUnionWalletFromWeb3 = require('./from-web3');

const makeUnionWalletMockWeb3 = (web3, unionSeed) => makeUnionWalletFromWeb3(web3, (entropy, address) => web3.eth.sign(entropy, address));

module.exports = makeUnionWalletMockWeb3;
