'use strict';

const { soliditySha3 } = require('web3-utils');
const bip39 = require('bip39');
const { stripHexPrefix, bufferToHex } = require('ethereumjs-util');
const { eztz } = require('eztz.js');

const TEZOS_NODE = require('./tezos-node');

const makeUnionWalletImpl = async (sign, unionSeed) => {
  const entropy = soliditySha3({
		t: 'bytes',
		v: bufferToHex(await bip39.mnemonicToSeed(unionSeed))
	});
	const mnemonic = bip39.entropyToMnemonic(stripHexPrefix(soliditySha3({
		t: 'bytes',
		v: await sign(entropy)
	})));
  const wallet = eztz.crypto.generateKeys(mnemonic);
	return wallet;
};

module.exports = makeUnionWalletImpl;
