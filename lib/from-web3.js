'use strict';

const makeUnionWalletImpl = require('./implementation');

const makeUnionWalletFromWeb3 = async (web3, signWithWeb3, unionSeed) => {
	const accounts = await web3.eth.getAccounts();
	if (!accounts || !accounts[0]) throw Error('Ethereum account not unlocked');
	const [ address ] = accounts;
	return makeUnionWalletImpl((entropy) => signWithWeb3(entropy, address), unionSeed);
};

module.exports = makeUnionWalletFromWeb3;
