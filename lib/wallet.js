'use strict';

const makeUnionWalletFromWeb3 = require('./from-web3');

const makeUnionWallet = (web3, unionSeed) => makeUnionWalletFromWeb3(web3, (entropy, address) => web3.eth.personal.sign(entropy, address));

module.exports = makeUnionWallet;
